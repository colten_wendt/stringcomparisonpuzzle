﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringComparisonPuzzle
{
    public static class ExtensionMethods
    {
        public static string GetString(this List<StringItem> stringItems)
        {
            var output = string.Empty;
            foreach (var stringItem in stringItems)
            {
                var xmlOpenTag = string.Empty;
                var xmlCloseTag = string.Empty;
                switch (stringItem.StringItemType)
                {

                    case StringItemType.Replacement:
                        xmlOpenTag = "<r>";
                        xmlCloseTag = "</r>";
                        break;
                    case StringItemType.Insertion:
                        xmlOpenTag = "<i>";
                        xmlCloseTag = "</i>";
                        break;
                    case StringItemType.Deletion:
                        xmlOpenTag = "<d>";
                        xmlCloseTag = "</d>";
                        break;
                    default:
                        break;
                }
                output = $"{output} {xmlOpenTag}{stringItem.Text}{xmlCloseTag}".Trim()
                    .Replace(@"</c> <c>", " ")
                    .Replace(@"</i> <i>", " ")
                    .Replace(@"</r> <r>", " ")
                    .Replace(@"</d> <d>", " ");
            }

            return output;
        }
    }
}