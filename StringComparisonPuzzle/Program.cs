﻿using System;

namespace StringComparisonPuzzle
{
    class Program
    {
        static void Main(string[] args)
        {
            var original = "so i saw something that reminded me of yesterday it was an article about";
            //var corrected = "So I saw something that reminded me of test test yesterday you yesterday. It was an about aliens.";
            var corrected = "So I saw something that reminded me of you yesterday. It was an article about aliens.";
            var list = StringItemHandler.DetectChanges(original, corrected);
            Console.WriteLine(list.GetString());
        }
    }
}
