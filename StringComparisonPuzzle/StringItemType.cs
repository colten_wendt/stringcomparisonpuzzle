﻿namespace StringComparisonPuzzle
{
    public enum StringItemType
    {
        Match,
        Deletion,
        Insertion,
        Replacement
    }
}