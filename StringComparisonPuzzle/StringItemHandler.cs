﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringComparisonPuzzle
{
    public class StringItemHandler
    {
        public static List<StringItem> DetectChanges(string original, string corrected, List<StringItem> stringItemList = null)
        {
            if (stringItemList == null)
            {
                stringItemList = new List<StringItem>();
            }

            if (string.IsNullOrEmpty(original) && string.IsNullOrEmpty(corrected))
            {
                return stringItemList;
            }
            else if (string.IsNullOrEmpty(original) || string.IsNullOrEmpty(corrected))
            {
                stringItemList.Add(new StringItem($"{corrected}", string.IsNullOrEmpty(corrected) ? StringItemType.Deletion : StringItemType.Insertion));
                return stringItemList;
            }
            else if (original.Split(' ').ToList()[0] == corrected.Split(' ').ToList()[0])
            {
                stringItemList.Add(new StringItem(original.Split(' ')[0]));
                var newOriginal = RemoveFirstWord(original);
                var newCorrected = RemoveFirstWord(corrected);
                return DetectChanges(newOriginal, newCorrected, stringItemList);
            }
            else
            {
                var originalWordsOff = GetWordsOffScore(original, corrected);
                var correctedWordsOff = GetWordsOffScore(corrected, original);
                if (originalWordsOff == correctedWordsOff)
                {
                    //Replacement
                    var replacedText = corrected.Split(' ')[0];
                    var newOriginal = RemoveFirstWord(original);
                    var newCorrected = RemoveFirstWord(corrected);
                    stringItemList.Add(new StringItem(replacedText, StringItemType.Replacement));
                    return DetectChanges(newOriginal, newCorrected, stringItemList);
                }
                if (originalWordsOff > correctedWordsOff)
                {
                    //Insertion
                    var addedText = GetWordsUntilFirstMatch(corrected, original);
                    var newCorrected = TrimWordsUntilFirstMatch(corrected, original);
                    stringItemList.Add(new StringItem(addedText, StringItemType.Insertion));
                    return DetectChanges(original, newCorrected, stringItemList);
                }
                else
                {
                    //Deletion
                    var deletedText = GetWordsUntilFirstMatch(original, corrected);
                    var newOriginal = TrimWordsUntilFirstMatch(original, corrected);
                    stringItemList.Add(new StringItem(deletedText, StringItemType.Deletion));
                    return DetectChanges(newOriginal, corrected, stringItemList);
                }
            }
        }

        private static int GetWordsOffScore(string input, string original)
        {
            var count = 0;
            var newInput = input;
            bool found = false;
            while (!found)
            {
                if (original.Split(' ')[0] == newInput.Split(' ')[0])
                {
                    found = true;
                }
                else
                {
                    count++;
                    var trackingInput = string.Empty;
                    newInput = RemoveFirstWord(newInput);
                }
                if (string.IsNullOrEmpty(newInput))
                {
                    return Int32.MaxValue;
                }
            }
            return count;
        }

        private static string GetWordsUntilFirstMatch(string input, string original)
        {
            var output = string.Empty;
            var newInput = input;
            bool found = false;
            while (!found)
            {
                if (original.Split(' ')[0] == newInput.Split(' ')[0])
                {
                    found = true;
                }
                else
                {
                    output = $"{output} {newInput.Split(' ')[0]}".Trim();
                    newInput = RemoveFirstWord(newInput);
                }
            }
            return output;
        }

        private static string RemoveFirstWord(string input)
        {
            var trackingInput = string.Empty;
            for (int i = 1; i < input.Split(' ').Length; i++)
            {
                trackingInput = $"{trackingInput} {input.Split(' ')[i]}".Trim();
            }
            input = trackingInput.Trim();
            return input;
        }

        private static string TrimWordsUntilFirstMatch(string input, string original)
        {
            var output = string.Empty;
            var newInput = input;
            bool found = false;
            while (!found)
            {
                if (original.Split(' ')[0] == newInput.Split(' ')[0])
                {
                    found = true;
                    output = newInput;
                }
                else
                {
                    output = $"{output} {newInput.Split(' ')[0]}".Trim();
                    newInput = RemoveFirstWord(newInput);
                }
            }
            return output;
        }
    }
}
