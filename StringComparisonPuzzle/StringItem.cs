﻿namespace StringComparisonPuzzle
{
    public class StringItem
    {
        public string Text { get; set; }
        public StringItemType StringItemType { get; set; }

        public StringItem() { }

        public StringItem(string text, StringItemType stringItemType = StringItemType.Match)
        {
            Text = text;
            StringItemType = stringItemType;
        }
    }
}
